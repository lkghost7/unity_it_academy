﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Hop;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
    [SerializeField] private GameObject m_Platform;
    [SerializeField] private int m_RandomSeed = 12345678;
    private  List<GameObject> platforms = new List<GameObject>();
    private readonly int[] _indexesJumpPlatforms = { 5, 11, 15 };
    private readonly int[] _indexesFastPlatforms = { 7, 13, 17 };
    private bool _isJumpPlatform;
    private bool _isFastPlatform;
    private int _currentIndexPlatform = 1;

    void Start()
    {
       // ВНИМАНИЕ РАНДОМ ВЫДАЕТ ОДИН И ТОТ ЖЕ РАНДОМ
      // Убрать если нужен рандом
     //   Random.InitState(m_RandomSeed);
        platforms.Add(m_Platform);
        for (int i = 0; i < 25; i++)
        {
            // сразу сприсоединяем к родителю то что создали
            GameObject obj = Instantiate(m_Platform, transform);
            
            var position = Vector3.zero;
            position.z = 2 * (i + 1);
            position.x = Random.Range(-1, 2);
            obj.transform.position = position;
            obj.name = $"Platform_{i}";
            if (IsIndexJumpPlatform(i))
            {
                HopHelper.SetMaterial(obj);
                obj.gameObject.name = "jumpPlatform";
            }
            if (IsIndexFastPlatform(i))
            {
                HopHelper.SetMaterialAndTexture(obj, "SpeedPlatformMat", "SpeedPlatformTex");
                obj.gameObject.name = "fastPlatform";
            }
            platforms.Add(obj);
        }
    }

    // TODO сделать метод
    public bool IsJumpPlatform()
    {
        return _isJumpPlatform;
    }
    public bool IsFastPlatform()
    {
        return _isFastPlatform;
    }
    public bool IsBallOnPlatform(Vector3 position)
    {
        position.y = 0f;
        // TODO здесь  можно считать и по номерам другим способом
        GameObject nearestPlatform = platforms[0];
        // начальное значение будет уменьшаться
        // на величину уже бывших в употреблении платформ
        for (int i = _currentIndexPlatform, count =  _currentIndexPlatform + 4; i < count; i++)
        {
            if (platforms[i].transform.position.z + 0.5f < position.z)
            {
                continue;
            }
            if (platforms[i].transform.position.z - position.z > 1f)
            {
                continue;
            }
            nearestPlatform = platforms[i];
            _currentIndexPlatform++;
            _isJumpPlatform = IsIndexJumpPlatform(i - 1) ? true : false;
            _isFastPlatform = IsIndexFastPlatform(i - 1) ? true : false;
            break;
        }
        float minX = nearestPlatform.transform.position.x - 0.5f;
        float maxX = nearestPlatform.transform.position.x + 0.5f;
        return position.x > minX && position.x < maxX;
    }

    private bool IsIndexJumpPlatform(int currentIndex)
    {
        return _indexesJumpPlatforms.Any(index => index == currentIndex);
    }
    private bool IsIndexFastPlatform(int currentIndex)
    {
        return _indexesFastPlatforms.Any(index => index == currentIndex);
    }
}
