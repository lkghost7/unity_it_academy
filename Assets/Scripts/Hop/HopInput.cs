﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopInput : MonoBehaviour
{
    public float Strafe { get; private set; }

    private float screenCenter;
        
    

    void Start()
    {
        screenCenter = Screen.width * 0.5f;
    }

    void Update()
    {
        if (!Input.GetMouseButton(0))
        {
            return;
        }

        var mousePosition = Input.mousePosition.x;
        if (mousePosition > screenCenter)
        {
            Strafe = (mousePosition - screenCenter) / screenCenter;
        }
        else
        {
            Strafe = 1 - mousePosition / screenCenter;
            Strafe *= -1f;
        }
    }
}
