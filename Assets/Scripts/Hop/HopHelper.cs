﻿using UnityEngine;

namespace Hop
{
    public class HopHelper : MonoBehaviour
    {
        public static void SetMaterial(GameObject obj)
        {
            var newMat = Resources.Load("FlatRocks", typeof(Material)) as Material;
            var tex = Resources.Load("Barriers") as Texture;
            var mRenderer = obj.GetComponent<Renderer>();
            if (newMat == null) return;
            newMat.mainTexture = tex;
            mRenderer.material = newMat;
        }
        
        public static void SetMaterialAndTexture(GameObject obj, string material, string texture)
        {
            var newMat = Resources.Load(material, typeof(Material)) as Material;
            var tex = Resources.Load(texture) as Texture;
            var mRenderer = obj.GetComponent<Renderer>();
            if (newMat == null) return;
            newMat.mainTexture = tex;
            mRenderer.material = newMat;
        }

        public static float GetRandomLenght()
        {
            var a = Random.Range(1, 4);
            var b = Random.Range(4, 6);
            return Random.Range(a, b);
        }
    }
}
