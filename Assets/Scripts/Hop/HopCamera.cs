﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopCamera : MonoBehaviour
{
    [SerializeField] private Transform m_Target;
    [SerializeField] private float m_Distance = 2f;
    [SerializeField] private float m_Height = 2f;
    
    void Update()
    {
       Vector3 position = new Vector3(0f, m_Height, m_Target.position.z - m_Distance);
       transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * 5f);
    }
}
