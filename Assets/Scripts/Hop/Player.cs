﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    [SerializeField] private AnimationCurve m_JumpCurve;
    // высота прыжка
    [SerializeField] private float m_JumpHeight = 1f;
    // длина прыжка
    [SerializeField] private float m_JumpDistance = 2f;
    // скорость в метрах в секунду - или 1 юнит в секунду
    [SerializeField] private float m_BallSpeed = 1f;
    [SerializeField] private HopInput m_Input;
    [SerializeField] private HopTrack m_Track;

    [SerializeField] private float heightForJumpPlatform = 3f;
    [SerializeField] private float speedForJumpPlatform = 1.2f;

    private bool _isJumpPlatform;

    private float iteration; // цикл прыжка
    private float startZ; // точка начала прыжка
    
    void Update()
    {
        var position = transform.position;
        // смещение
        position.x = Mathf.Lerp(position.x, m_Input.Strafe, Time.deltaTime);
        // прыжок
        position.y = m_JumpCurve.Evaluate(iteration) * m_JumpHeight;
        
        // движение вперед
        position.z = startZ + iteration * m_JumpDistance;

        transform.position = position;
        // увеличиваем счетчик прыжка
        iteration += Time.deltaTime * m_BallSpeed;
        if (iteration < 1f)
        {
            return;
        }

        iteration = 0f;
        startZ += m_JumpDistance;
        if (m_Track.IsBallOnPlatform(transform.position))
        {
            if (m_Track.IsJumpPlatform())
            {
                m_JumpHeight = 5f;
                m_BallSpeed = 1.2f;
            }
            else
            {
                SetDefaultFields();
            }
            if (m_Track.IsFastPlatform())
            {
                m_JumpDistance = 4f;
            }
            else
            {
                SetDefaultJumpDistance();
            }
            return;
        }
        //иначе делаем проигрыш
        SetDefaultFields();
        SetDefaultJumpDistance();
        Debug.Log("====== COBOL Установлен на ваш компьютер ВЫ ПРОИГРАЛИ !!! =======");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void SetDefaultFields()
    {
        m_JumpHeight = 1f;
        m_BallSpeed = 1f;
    }
    private void SetDefaultJumpDistance()
    {
        m_JumpDistance = 2f;
    }
}
