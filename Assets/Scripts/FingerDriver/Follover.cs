﻿using System;
using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;

public class Follover : MonoBehaviour
{
    public PathCreator pathCreator;
    public float speed = 1;
    private float distanceTravelled;

    private void Start()
    {
//        transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
    }

    void Update()
    {
        distanceTravelled += speed * Time.deltaTime;
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
//        transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled);
//        transform.rotation = new Quaternion(0f, 0f, 0f, 100f);
    }
}
