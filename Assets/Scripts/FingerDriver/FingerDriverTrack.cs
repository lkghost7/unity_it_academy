﻿using UnityEngine;

public class FingerDriverTrack : MonoBehaviour
{
    private class TrackSegment
    {
        public Vector3[] Points;
        public int numberSegment;

        public bool IsPointInSegment(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(point, Points[0], Points[1], Points[2]);
        }
    }

    [SerializeField] private LineRenderer m_LineRenderer;
    [SerializeField] private bool m_ViewDebug;

    private Vector3[] corners;
    private TrackSegment[] segments;
    private bool isFirstTime = true;
    private int currentSegment;

    private void Start()
    {
        //заполняем массив опорных точек
        corners = new Vector3[transform.childCount];

        for (int i = 0; i < corners.Length; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            corners[i] = obj.transform.position;
            obj.GetComponent<MeshRenderer>().enabled = false;
        }

        //настраиваем LineRenderer
        m_LineRenderer.positionCount = corners.Length;
        m_LineRenderer.SetPositions(corners);

        //из LineRenderer запекаем меш (сетку треугольников)
        Mesh mesh = new Mesh();
        m_LineRenderer.BakeMesh(mesh, true);

        //создаем массив сегментов трассы
        segments = new TrackSegment[mesh.triangles.Length / 3];

        int segmentCounter = 0;
        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            segments[segmentCounter] = new TrackSegment {numberSegment = segmentCounter, Points = new Vector3[3]};
            segments[segmentCounter].Points[0] = mesh.vertices[mesh.triangles[i]];
            segments[segmentCounter].Points[1] = mesh.vertices[mesh.triangles[i + 1]];
            segments[segmentCounter].Points[2] = mesh.vertices[mesh.triangles[i + 2]];
            segmentCounter++;
        }

        if (!m_ViewDebug)
        {
            return;
        }

        foreach (var segment in segments)
        {
            foreach (var point in segment.Points)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = point;
                sphere.transform.localScale = Vector3.one * 0.1f;
            }
        }
    }

    public bool IsPointInTrack(Vector3 point)
    {
        for (int i = 0; i < segments.Length; i++)
        {
            if (isFirstTime)
            {
                foreach (var trackSegment in segments)
                {
                    Debug.Log("Текущий сегмент: " + trackSegment.numberSegment);
                    if (trackSegment.IsPointInSegment(point))
                    {
                        Debug.Log("Наше местонахождение в сегменте: " + trackSegment.numberSegment);
                        isFirstTime = false;
                        currentSegment = trackSegment.numberSegment;
                        return true;
                    }
                }
            }
            else
            {
                for (int j = currentSegment; j < currentSegment + 3; j++)
                {
                    if (currentSegment + 1 != segments.Length)
                    {
                        var segment = segments[j];
                        Debug.Log("Номер сканируемого сегмента" + segment.numberSegment);
                        if (segment.IsPointInSegment(point))
                        {
                            currentSegment = segment.numberSegment;
                            if (currentSegment == segments.Length)
                            {
                                currentSegment = 0;
                            }
                            return true;
                        }
                    }
                    else
                    {
                        currentSegment = 0;
                        return true;
                    }
                }
            }
        }
        return false;
    }
}