﻿using System;
using TMPro;
using UnityEngine;

public class ScoreLevel : MonoBehaviour
{
    public GameObject textScore;
    public GameObject textLevel;
    public static int _score;
    private static int _level;
    private static bool _isChangeScore;
    private static bool _isChangeLevel;


    private void Start()
    {
        _score = 0;
        _level = 1;
    }

    public static void setScore()
    {
        _score += 10;
        _isChangeScore = true;
    }

    private void Update()
    {
        if (_isChangeScore)
        {
            textScore.GetComponent<TextMeshPro>().SetText(_score.ToString());
            _isChangeScore = false;
            _isChangeLevel = true;
        }

        int scoreDivision = _score % 3;
        if (_score > 0 && scoreDivision == 0 && _isChangeLevel)
        {
            LevelUP();
            FingerDriverPlayer.SpeedUp();
        }
    }

    private void LevelUP()
    {
        _isChangeLevel = false;
        _level++;
        textLevel.GetComponent<TextMeshPro>().SetText("Level " + _level.ToString());
    }
}