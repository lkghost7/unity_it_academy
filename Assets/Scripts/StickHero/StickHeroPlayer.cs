﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickHeroPlayer : MonoBehaviour
{
    [SerializeField] private StickHeroController m_Controller;

    private bool isMoving;
    private float targetPosition;
    private bool isFalling;
    
    /// <summary>
    /// Начинаем движение игрока
    /// </summary>
    /// <param name="position">целевая позиция</param>
    /// <param name="isFall"> нужно ли падать по достижению позиции</param>
    public void StartMovement(float position, bool isFall)
    {
        targetPosition = position;
        isMoving = true;
        isFalling = isFall;
    }

    private void Update()
    {
        if (isMoving)
        {
            transform.Translate(Vector3.right * Time.deltaTime);

            if (transform.position.x < targetPosition)
            {
                return;
            }

            isMoving = false;

            if (!isFalling)
            {
                m_Controller.OnPlayerStop();
            }
        }

        if (!isFalling)
        {
            return;
        }
        
        //падаем
        transform.Translate(Time.deltaTime * 2f * Vector3.down);

        if (transform.position.y <= -1f)
        {
            isFalling = false;
            m_Controller.OnPlayerDie();
        }
        

    }
}
